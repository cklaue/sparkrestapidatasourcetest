# Spark REST API TEST

## PURPOSE

Test how to talk to REST APIs using Spark.
## TODO

Get the  jar from [here](https://github.com/sourav-mazumder/Data-Science-Extensions/tree/master/spark-datasource-rest
)
and make sure you put that on the machie you are submitting the app on.

## HOWTO
```
spark2-submit \
--jars spark-datasource-rest_2.11-2.1.0-SNAPSHOT.jar --packages org.scalaj:scalaj-http_2.10:2.3.0 \
--class io.phdata.examples.SparkExample \
--master local[8] \
SparkExample-1.0-SNAPSHOT-jar-with-dependencies.jar
```