package io.phdata.examples

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._


object SparkExample extends App {

  override def main(args: Array[String]) = {

    val spark = SparkSession
      .builder().appName("restAPI").getOrCreate()

    import spark.implicits._

    val sc = spark.sparkContext

    // define the basic URL
    val aisleURL = "https://api.aislelabs.com/api/rest/metrics/visitlist.json?tdid=1223&user_key=c553480fda09feff6e13dbe88843b956&startTime=2017-11-29%2010:00:00&endTime=2017-11-30%2010:00:00"

    // Define parameters which will be queried, this could be different buildings or timestamps for oxford.
    val aisleInput0 = ("10", "true")
    val aisleInput1 = ("100", "false")

    val aisleInputRdd = sc.parallelize(Seq(aisleInput0,aisleInput1))

    val aisleinputKey1 = "maxPhones"
    val aisleinputKey2 = "connectedOnly"

    // save them as a temp view
    aisleInputRdd.toDF(aisleinputKey1, aisleinputKey2).createOrReplaceTempView("aisleinputtbl")

    spark.sql("select * from aisleinputtbl").show()

    val parmg = Map("url" -> aisleURL, "input" -> "aisleinputtbl", "method" -> "GET", "readTimeout" -> "60000", "connectionTimeout" -> "5000", "partitions" -> "10")

    // save the results of the REST API call in a DF
    val aisleDf = spark.read.format("org.apache.dsext.spark.datasource.rest.RestDataSource").options(parmg).load()

    aisleDf.printSchema

    aisleDf.createOrReplaceTempView("aisletbl")

    spark.sql("select * from aisletbl").show()

    // get the average time of how long they spent inside
    aisleDf.select("output.response.flowVisitObjects.inOutDurationMillis").where($"connectedOnly" === "true")
      .withColumn("inOutDurationMillis", explode($"inOutDurationMillis"))
      .select("inOutDurationMillis.inDurationMillis").select(mean($"inDurationMillis")).show()

  }
}
